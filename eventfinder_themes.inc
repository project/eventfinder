<?php
/**
 * @defgroup eventfinder_themes EventFinder Themeable Functions
 */
/**
 * Main eventfinder page
 * @ingroup eventfinder_themes
 * @args - POST variables
 */
function theme_eventfinder_main($content){
  $output = '<div id="eventfinder">';
  $output .= $content;
  $output .= "</div>";
  return theme('page', $output);
}
/**
 * Separate method for displaying the main search
 * @ingroup eventfinder_themes
 * @args - POST variables
 */
function theme_eventfinder_main_page($content){
  $output = '<div id="eventfinder">';
  $output .= $content;
  $output .= "</div>";
  return theme('page', $output);
}
/**
 * Method for showing options within a node
 * @ingroup eventfinder_themes
 * @args - POST variables
 */
function theme_eventfinder_nodeoptions($content){
  $output = '<div id="eventfinder">';
  $output .= $content;
  $output .= "</div>";
  return $output;
}
/**
 *
 * Description goes here
 *
 * @ingroup groupname
 * @return description of what is returned
 *
 */
function theme_eventfinder_search_control($content, $name = '') {
  $output = '<div class="ef_search_control ' . $name . '">';
  $output .= $content;
  $output .= '</div>';
  return $output;
}
/**
 * Displays Host an Event Page
 *
 * @ingroup eventfinder_themes
 * @args - POST variables
 *
 */
function theme_eventfinder_host($content){
  $output = '<div class="ef_host_event">';
  $output .= $content;
  $output .= '</div>';
  return theme('page', $output);
}
/**
 * Displays Host an Event Page
 *
 * @ingroup eventfinder_themes
 * @args - POST variables
 *
 */
function theme_eventfinder_myevents($content){
  $output .= '<div id="eventfinder">';
  $output .= $content;
  $output .= '</div>';
  return theme('page', $output);
}

/**
 * Displays MyEvents - Registered Events
 *
 * @ingroup eventfinder_themes
 * @content 
 *
 */
function theme_eventfinder_my_events_reg($content){
  $output .= '<div id="ef_reg_events">';
  $output .= '<span>Registered Events</span>' . '<br>';
  $output .= $content;
  $output .= '</div>';
  return $output;
}

/**
 * Displays MyEvents - Hosted Events
 *
 * @ingroup eventfinder_themes
 * @content 
 *
 */
function theme_eventfinder_my_events_host($content){
  $output .= '<div id="ef_hosted_events">';
  $output .= '<span>Hosted Events</span>' . '<br>';
  $output .= $content;
  $output .= '</div>';
  return $output;
}

/**
 *
 * EevntFinder descriptions and instructions
 *
 * @ingroup eventfinder_themes
 * @return DIV containing instructions for users
 *
 */
function theme_eventfinder_desc ($arg1) {
  $output .= '<div id="ef_desc">';
  $output .= $arg1;
  $output .= '</div>';
  return $output;
}

/**
 *
 * Displays a node in the search results page
 *
 * @ingroup eventfinder_themes
 * @return fully formatted node
 *
 */
function theme_eventfinder_node_preview($node, $teaser = FALSE, $page = FALSE, $my = FALSE) {
  // TODO: Clean up this theme, remove business logic
  global $user;
  $output .= '<div class="ef_node">';
  if (module_exist('taxonomy')) {
    $terms = taxonomy_link('taxonomy terms', $node);
  }
  if ($page == 0) {
    $output .= '<h2 class="title">'. l(check_plain($node->title), 'eventfinder/search_events/' . $node->nid) .'</h2> by '. format_name($node);
  } else {
    $output .= 'by '. format_name($node);
  }
  if (count($terms)) {
    $output .= ' <small>('. theme('links', $terms) .')</small><br />';
  }
  $output .= $node->teaser;
  // checking to see if this node is being previewed in the myevents section
  if(!$my){
    $links[] = l(t('calendar'), 'event/'. format_date($node->event_start, 'custom', 'Y/m/d'));
    $links[] = l(t('new search'), 'eventfinder');
    $at_cap = $node->registered_users < $node->max_reg;
    $u_reg = eventfinder_check_reg($node->nid);
    if($node->enable_reg == 1){
      if(!eventfinder_check_host($node->nid)){
        $links[] = l(t(eventfinder_reg_users($node, TRUE)), 'eventfinder/search_events/' . $node->nid);
        if(($node->registered_users >= $node->max_reg) && $node->max_reg > 0){
          $links[] = t("event is at capacity");
        } else if(!$u_reg && $node->max_reg > 0) {
				  if(variable_get('ef_reg_opt', 0) === 0){
				    $links[] = l(t('Register for this Event'), 'eventfinder/search_events/' . $node->nid . '/register/');
				  } else {
				    $links[] = l(t('Register for this Event'), 'eventfinder/register/' . $node->nid);
				  } 
       } else if($node->max_reg > 0) {
          $links[] = t("you are registered for this event");
        }
      } else {
        $links[] = l(t(eventfinder_reg_users($node, TRUE)), 'eventfinder/search_events/' . $node->nid);
      }
    } else {
      $links[] = l('event details', 'eventfinder/search_events/' . $node->nid);
    }
  } else {
      $links[] = l('event details', 'eventfinder/search_events/' . $node->nid);
  }
  $output .= '<div class="links">'. theme('links', $links) .'</div>';
  $output .= '</div>';
  return $output;
}
/**
 *
 * Displays a node in the details page
 *
 * @ingroup eventfinder_themes
 * @return fully formatted node
 *
 */
function theme_eventfinder_node_details($node, $teaser = FALSE, $page = FALSE, $links = TRUE) {
  $output = '<div id="eventfinder">';
  $output .= '<div class="ef_node">';
  if (module_exist('taxonomy')) {
    $terms = taxonomy_link('taxonomy terms', $node);
  }
  $output .= '<h2 class="title">'. check_plain($node->title) .'</h2> by '. format_name($node);
  if (count($terms)) {
    $output .= ' <small>('. theme('links', $terms) .')</small><br />';
  }
  $output .= $node->body;
  node_invoke_nodeapi($node, 'view', $teaser, $page);

  $new_links[] = l(t('calendar'), 'event/'. format_date($node->event_start, 'custom', 'Y/m/d'));
  $new_links[] = l(t('new search'), 'eventfinder');
  $new_links[] = l(t(eventfinder_get_registered($node->nid)), 'eventfinder');
  
  $output .= '<div class="links">'. theme('links', $node->links) .'</div>';
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}
/**
 * EventFinder options 'block'
 * @ingroup eventfinder_themes
 */
function theme_eventfinder_options ($arg1) {
  $output .= '<div id="ef_options">';
  $output .= $arg1;
  $output .= '</div>';
  form_group('EventFinder Options', $arg1);
  return $output;
}

/**
 *
 * Registration page
 *
 * @ingroup eventfinder_themes
 *
 */
function theme_eventfinder_registration_form($title, $content) {
  $output = '<div id="ef_reg_page">';
  $info .= 'You are about to register for the following event:<br/>';   
  $info .= 'Title: ' . $title . '<br>';   
  $info .= 'Please fill out the following form to proceed.';   
  $output .= theme('eventfinder_desc', $info);
  $output .= $content;
  $output .= '</div>';
  return $output;
}
?>